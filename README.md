# Accept

A go library to parse the Accept HTTP Header

It was written because of this toot: https://uwu.social/@Joshix/108424918060682309

#### Usage:

```golang
import "codeberg.org/u0nel/accept"

func HandleRequest(w http.ResponseWriter, r *http.Request) {
	// we're using a slice instead of a map[string]func,
	// because we want to be able to rank the types
	possible_types := []string{
				"text/html",
				"application/json+ld",
				"text/plain",
				}
	switch accept.ServeType(possible_types, r.Header.Get("Accept")) {
	case "text/html":
		// write html
	case "application/json+ld":
		// write jsonld
	case "text/plain":
		// write plaintext
	default:
		// only if accept header exists and none matches one of the possible types
		http.Error(w, "Could not serve requested Type", http.StatusNotAcceptable)
	}
}
```

Example:  
[codeberg.org/uonel/useragent](https://codeberg.org/uonel/useragent)
